import sys

if len(sys.argv) > 1:
    print('Got ', end='')
    INPUT_SUM = 0
    i = 1
    while i < len(sys.argv)-1:
        if i != len(sys.argv):
            print(sys.argv[i], end=', ')
            INPUT_SUM = INPUT_SUM + float(sys.argv[i])
            i = i + 1
    print(sys.argv[i], end='. ')
    INPUT_SUM += float(sys.argv[i])
    print('Sum is ' + str(INPUT_SUM), end='')
else:
    print('Please input int after file name')
