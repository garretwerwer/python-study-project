# Python studying project of Denis Ahapau



 1. [Introduction](#intro)
 1. [Task 1](#task1)
 1. [Task 2](#task2)
 1. [Task 3](#task3)
 1. [Task 4](#task4)
 1. [Task 5](#task5)


----------
### <a name="intro"></a> Introduction

This repo is for storing and checking of home work

----------
### <a name="task1"></a> Task 1

Get input arguments (sys.argv) and print them and their sum:
Input: 1 2 5 6
Output: Got 1, 2, 5, 6. Sum is 14.

### <a name="task2"></a> Task 2
Get one input string argument and cut one word from the beginning and from the end:
Input: apple orange tomato potato
Output: orange tomato

### <a name="task3"></a> Task 3
Get one input string argument and print its letters in table format: 3 letters per line delimited by 4 spaces:
Input: comprehensive
Output:

c    o    m

p    r    e

h    e    n    

s    i    v

e

### <a name="task4"></a> Task 4
Get 2 numbers as input arguments and print result of devision with 2 fractional digits:
Input: 4 5
Output: 0.80

### <a name="task5"></a> Task 5
Get list arguments in sys.argv and return all of them as one sentence starting with Capital letter and ending with dot.
Input: "tom was good boy" "alice likes apples"
Output: Tom as good boy. Alice likes apples.
